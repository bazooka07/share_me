<?php
if(!defined('PLX_ROOT')) { exit; }

/**
 * Debuggers.
 *
 * @link https://developers.facebook.com/tools/debug/
 * @link http://cards-dev.twitter.com/validator
 * */

/*
 * # Some useful links #
 * @link http://ogp.me or http://opengraphprotocol.org/. Used by Facebook, Google+, Pinterest, Diaspora, ...
 * @link https://developers.facebook.com/docs/sharing/webmasters  ### en français ###
 * @link https://github.com/Ohax/Boutons-sociaux-sans-tracking
 *
 * @link http://ohax.fr/ajouter-les-boutons-de-partage-des-reseaux-sociaux-sans-le-tracking-de-vos-visiteurs
 * @link https://github.com/adamfairhead/webicons
 * @link https://dev.twitter.com/web/tweet-button/web-intent
 * @link https://dev.twitter.com/cards/overview
 * @link https://developers.facebook.com/docs/sharing/webmasters
 * @link https://developers.pinterest.com/docs/rich-pins/articles/
 * @link https://wiki.diasporafoundation.org/FAQ_for_web_developers
 * @link https://github.com/sharetodiaspora/sharetodiaspora.github.io
 *
 * # Cookie consents in regard of the E.U. law : #
 * @link http://www.cookiechoices.org/
 * @link https://cookieconsent.insites.com/
 * @link https://github.com/insites/cookieconsent -> https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.4/cookieconsent.min.js
 * @link https://github.com/dobarkod/cookie-banner -> https://cdnjs.cloudflare.com/ajax/libs/cookie-banner/1.1.1/cookiebanner.min.js
 * @link http://www.cnil.fr/vos-obligations/sites-web-cookies-et-autres-traceurs/outils-et-codes-sources/les-boutons-sociaux/
 * @link https://www.cnil.fr/fr/site-web-cookies-et-autres-traceurs
 * */


/**
 * This plugin adds a toolbar with buttons for sharing in the social networks.
 * It uses 3 hooks for PluXml :
 *  - plxMotorDemarrageEnd
 *  - ThemeEndHead
 *  - ThemeEndBody
 *
 * And it creates a hook for themas :
 *  - share_me
 *
 * @author: Jean-Pierre Pourrez aka bazooka07
 * @version: 1.3.0
 *
 * */
class share_me extends plxPlugin {

	const HOOKS = array(
		'plxMotorDemarrageEnd',
		'ThemeEndHead',
		'ThemeEndBody',
		'share_me',
	);
	const COOKIE_VERSION = '3.1.1';
	const COOKIE_JS_URL = 'https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/'.self::COOKIE_VERSION.'/cookieconsent.min.js';
	const COOKIE_CSS_URL = 'https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/'.self::COOKIE_VERSION.'/cookieconsent.min.css';
	const EXTRAS = 'tags chapo cookie';
	/** A pattern for finding an image in a fragment of HTML for an article or a static page. */
	const PATTERN_IMG = '@<img\s.*src="([^"<>]*)".*/?>@';
	private $mode = false;
	private $buttonsShow = false;

	/**
	 * A list of social networks we know, with theirs URL and query.
	 * */
	const NETWORKS = array(
		'twitter'	=> array('https://twitter.com/intent/tweet?text=#title#&via=#account#&url=#url#&hashtags=#tags#', 480, 750),
		'facebook'	=> array('https://www.facebook.com/sharer.php?u=#url#&t=#title#', 500, 500),
		'google_p'	=> array('https://plus.google.com/share?url=#url#&hl=#lang#', 450, 600),
		'linkedin'	=> array('https://www.linkedin.com/shareArticle?mini=true&url=#url#&title=#title#', 550, 700),
		'pinterest'	=> array('https://www.pinterest.com/pin/create/button/?url=#url#&media=#image#&description=#description#', 450, 850),
		'reddit'	=> array('http://reddit.com/submit?url=#url#&feature=share&title=#description#', 450, 450),
		'tumblr'	=> array('http://www.tumblr.com/share/video?embed=#url#&feature=share&caption=#title#', 450, 450),
		'digg'		=> array('href="http://www.digg.com/submit?url=#url#', 450, 450),
		'flattr'	=> array('https://flattr.com/submit/auto?user_id=&title=#title#&url=#url#', 450, 450),
		'stumbleupon'=> array('http://www.stumbleupon.com/submit?url=#url#&title=#title#', 450, 450),
		'vkontakte'	=> array('http://vkontakte.ru/share.php?url=#url#', 450, 450),
//		'yummly'	=> array('http://www.yummly.com/urb/verify?url=#url#&title=#title#', 450 ,450),
		'buffer'	=> array('https://bufferapp.com/add?url=#url#&text=#title#', 450, 450),
		'diaspora'	=> array('#diaspora/?title=#title#&url=#url#&notes=#description#', 500, 800),
		'mail'		=> array('mailto:?subject=#title#&body=#url#', false, false),
		'print'		=> false
	);

	/**
	 * Some network require an account.
	 * */
	const ACCOUNT_NETWORKS = array('twitter', 'facebook');

	public function __construct($default_lang) {

		# appel du constructeur de la classe plxPlugin (obligatoire)
		parent::__construct($default_lang);

		# droits pour accéder à la page config.php du plugin
		if(!defined('PLX_ADMIN')) {
			foreach(self::HOOKS as $hook) {
				parent::addHook($hook, $hook);
			}
		} else {
			parent::setConfigProfil(PROFIL_ADMIN);
		}
	}

	/**
	 * Cleanup HTML tags and entities from a content of an article of a static page.
	 * Drops empty lines, too.
	 * */
	private function _washing($content) {
		return htmlspecialchars(
			implode(
				"\r\n",
				array_map(
					function($line) {
						return trim($line);
					},
					array_filter(
						explode(
							"\n",
							html_entity_decode(strip_tags($content))
						),
						function($line) {
							return !empty(trim($line));
						}
					)
				)
			),
			ENT_COMPAT,
			PLX_CHARSET
		);
	}

	/**
	 * Because some article hasn't a chapô, we attempt to get the first paragraph with some text
	 * in the content of this article. By extension, we try also with any fragment of HTML like a static page.
	 *
	 * @param $content fragment of HTML
	 * @param integer $minChars minimal account of characters in the paragraph
	 *
	 * @return string the text of the first valid paragraph clean up of image, empty lines, HTML entities, and so on.
	 * */
	private function _getFirstParagraph($content, $minChars=10) {

		$result = false;
		if(!empty($content) and preg_match_all('@<p[^>]*>(.*)</p>@isU', $content, $matches)) {
			# on capture le 1er paragraphe (<p..>) contenant au moins $minChars caractères de texte
			for($i=0; $i<count($matches[1]); $i++) {
				$buf = trim($this->_washing($matches[1][$i]));
				if(strlen($buf) > $minChars) {
					$result = $buf;
					break;
				}
			}
		}
		return $result;
	}

	/**
	 * Get picture from article or a static page.
	 * _searchMedia function is used if exploration of content an article or static page is required.
	 *
	 * @return an URL of a picture
	 * */
	private function _getImage($content) {
		if(preg_match_all($this->mediasPattern, $content, $matches, PREG_SET_ORDER)) {
			foreach($matches as $set1) {
				if(empty($set1[2]) and file_exists($set1[1])) {
					return $set1[1];
					break;
				}
			}
		}
		return false;
	}

	/**
	 * Adjust the correct localisation for sharing from the language used by PluXml.
	 *
	 * @return correct localisation for sharing
	 * */
	private function _getLocale($lang) {

		$specialLangs = array(
			'en' => 'en_US',
			'oc' => 'oc_FR'
		);
		return (array_key_exists($lang, $specialLangs)) ? $specialLangs[$lang] : $lang.'_'.strtoupper($lang);
	}

	/**
	 * @return tags of keywords of the current article or static page
	 * */
	private function _getTags($content) {
		if(!empty($result)) {
			return array_map(
				function($item) {
					return trim($item);
				},
				explode(',', $result)
			);
		} else {
			return false;
		}
	}

	private function _getStaticContent() {
		// Cf. plxShow::staticContent()
		global $plxMotor;

		$idStat = $plxMotor->cible;
		if($plxMotor->aStats[$idStat]['readable'] != 1) { return false; }

		$filename = implode('', array(
			PLX_ROOT,
			$plxMotor->aConf['racine_statiques'],
			$idStat.'.'.$plxMotor->aStats[$idStat]['url'].'.php'
		));

		if(!is_readable($filename)) { return false; }

		ob_start();
		require $filename;
		$output = ob_get_clean();
		// eval($plxMotor->plxPlugins->callHook('plxShowStaticContent'));
		return $output;
	}


	/**
	 * Get the icon of a sharing to display for a social network.
	 * @param name of the social network
	 *
	 * @return html tag for icon with alternative text. The icon is in SVG format.
	 * */
	public function iconNetwork($network) {
		$src = PLX_PLUGINS.__CLASS__.'/icons/';
		$src .= ($network != 'google_p') ? $network : 'googleplus';
		$src .= '.svg';
		$alt = ($network != 'google_p') ? ucfirst($network) : 'Google +';
		$data = (!in_array($network, array('mail', 'print'))) ? ' data="'.$network.'"' : '';
		return <<< EOT
<img src="$src" alt="$alt" title="$alt"$data />
EOT;
	}

	public function demarrage(&$plxMotor) {
		$this->root = $plxMotor->racine.$plxMotor->aConf['racine_plugins']. __CLASS__ .'/';
		$this->mediasPattern = $mask = '@<img(?:\s+[^>]+)?\s+src="((?:(https?://)|'.$plxMotor->aConf['medias'].')[^"]+)"[^>]*>@';

		# default values
		$title = $plxMotor->aConf['title'];
		$description = $plxMotor->aConf['description'];
		$author = false;
		$published_time = date(DATE_ISO8601);
		$tags = false;

		$this->mode = $plxMotor->mode;
		switch($plxMotor->mode) {
			case 'article' : /* ------------------ article ------------------- */
				# We display just one article
				$article = $plxMotor->plxRecord_arts;

				# title
				$title = $article->f('title');

				# description
				if (!empty(trim($article->f('chapo')))) {
					$description = strip_tags($article->f('chapo'));
				} elseif(!empty((trim($article->f('meta_description'))))) {
					$description = $article->f('meta_description');
				} else {
					$firstP = self::_getFirstParagraph($article->f('content'));
					if(!empty($firstP)) {
						$description = strip_tags($firstP);
					}
				}

				# image
				$filename = trim($article->f('thumbnail'));
				if(!empty($filename) and file_exists($filename)) {
					$img = str_replace('.tb.', '.', $filename);
				} else {
					$img = self::_getImage($article->f('content'));
				}

				# published_time
				if (preg_match('/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/', $article->f('date'), $k)) {
					$published_time = date(DATE_ISO8601, mktime($k[4], $k[5], 0, $k[2], $k[3], $k[1]));
				}

				# author
				$idAuthor = $article->f('author');
				$author = (isset($plxMotor->aUsers[$idAuthor]['name'])) ? $plxMotor->aUsers[$idAuthor]['name'] : L_ARTAUTHOR_UNKNOWN;

				# tags
				$tags = self::_getTags($article->f('tags'));

				# url
				$idArt = intval($article->f('numero'));
				$urlArt = $article->f('url');
				$url = $plxMotor->urlRewrite("?article{$idArt}/{$urlArt}");
				break;
			case 'static' : /* ----------------- static page ------------------- */
				# We display just one static page
				$idStatic = $plxMotor->cible;
				if($plxMotor->aStats[$idStatic]['readable'] == 1) {
					$pageStatic = $plxMotor->aStats[$idStatic];

					$title = $pageStatic['name'];

					if (!empty(trim($pageStatic['meta_description']))) {
						$description = $pageStatic['meta_description'];
					} else {
						$filename = implode('', array(
							PLX_ROOT,
							$plxMotor->aConf['racine_statiques'],
							$plxMotor->cible.'.'.$plxMotor->aStats[$plxMotor->cible]['url'].'.php'
						));
						$content = (is_readable($filename)) ? file_get_contents($filename) : false;
						if(!empty($content)) {
							$firstP = $this->_getFirstParagraph($content);
							if(!empty($firstP)) {
								$description = strip_tags($firstP);
							}

							# image
							$img = self::_getImage($content);
						}
					}

					$filename = $plxMotor->aConf['racine_statiques'].$idStatic.'.'.$pageStatic['url'].'.php';
					$published_time = date(DATE_ISO8601, filemtime($filename));

					# tags
					$tags = self::_getTags($pageStatic['meta_keywords']);

					# Look at plxShow::staticList()
					$url = $pageStatic['url'];
					if($url[0]=='?') # url interne commençant par ?
						$url = $plxMotor->urlRewrite($url);
					elseif(plxUtils::checkSite($url,false)) # url externe en http ou autre
						$result = $url;
					else # url page statique
						$url = $plxMotor->urlRewrite('?static'.intval($idStatic).'/'.$url);
				} else {
					return;
				}
				break;
			default:
				return;
		}

		if(empty($img)) {
			$img = $this->getParam('media');
		}
		if(empty($img) or !file_exists($img)) {
			$img = 'core/admin/theme/images/theme.png';
		}
		$imgUrl = false;
		if(!empty($img)) {
			if(!preg_match('@^\w+://@', $img)) {
				$imgUrl = $plxMotor->racine.$img;
				$imgSizes = getimagesize(PLX_ROOT.$img);
			} else {
				$imgUrl = $img;
			}
		}

		$accountTwitter = $this->getParam('account_twitter');
		$this->defs = array(
			'og' => array(
				'title'			=> str_replace(str_split('_|'), ' ', plxUtils::strCheck($title)),
				'type'	 		=> 'article',
				'image'			=> $imgUrl,
				'url'			=> $url,
				'description'	=> $description,
				'locale'		=> $this->_getLocale($plxMotor->aConf['default_lang']),
				'site_name'		=> str_replace(str_split('_|'), ' ', $plxMotor->aConf['title']),
				'video'			=> false # coming soon ...
			),
			'article' => array(
				'published_time'=> $published_time,
				'author'		=> $author,
				'section'		=> false, # coming soon ...
				'tag'			=> $tags
			),
			# https://dev.twitter.com/cards/markup
			'twitter' => array(
				'card'			=> 'summary',
				// 'title'			=> $title,
				// 'image'			=> $imgUrl,
				// 'description'	=> $description,
				'site'			=> $accountTwitter
			),
			'fb' => array(
				'app_id'		=> $this->getParam('account_facebook') // https://developers.facebook.com/docs/sharing/webmasters

			)
		);
		if(!empty($imgSizes)) {
			$this->defs['og']['image:width'] = $imgSizes[0];
			$this->defs['og']['image:height'] = $imgSizes[1];
		}
	}

	private function _printCookieConsent() {

		if(strpos($this->getParam('extras'), 'cookie') !== false) {
			# https://cookieconsent.insites.com/documentation/javascript-api/
			$lang = $this->default_lang;
			$href = 'https://www.cookiechoices.org/intl/'.$lang;
			$staticUrl = $this->getParam('cookie_policy');
			if(!empty($staticUrl)) {
				global $plxShow;
				if(isset($plxShow)) {
					$href = $plxShow->plxMotor->urlRewrite('?static'.$staticUrl);
				}
			}
?>

	<script src="<?= self::COOKIE_JS_URL; ?>"></script>
	<script type="text/javascript">
		window.addEventListener('load', function(){
			window.cookieconsent.initialise({
				palette: {
					popup:	{ background: '#252e39' },
					button:	{ background: '#14a7d0' }
				},
				theme: 'classic',
				content: {
					message: "<?php $this->lang('L_CC_MESSAGE'); ?>",
					dismiss: "<?php $this->lang('L_CC_DISMISS'); ?>",
					'link': "<?php $this->lang('L_CC_LINK'); ?>",
					href: '<?= $href; ?>'
				}
			})
		});
	</script>
<?php
		}
	}

	/* ================================= Hooks ============================================ */

	const PLX_MOTOR_DEMARRAGE_END_CODE = <<< 'PLX_MOTOR_DEMARRAGE_END_CODE'
<?php $this->plxPlugins->aPlugins['##CLASS##']->demarrage($this); ?>
PLX_MOTOR_DEMARRAGE_END_CODE;

	/**
	 * Prepare the head of the page of an article or static page for Open Graph protocol.
	 *
	 * @return void
	 * */
	public function plxMotorDemarrageEnd() {
		echo str_replace('##CLASS##', __CLASS__, self::PLX_MOTOR_DEMARRAGE_END_CODE);
	}

	/**
	 * Add meta tags in the head of the HTML page for Open Graph protocol.
	 *
	 * @return void
	 * */
	public function ThemeEndHead() {
		# print every no-empty meta-tag listed above
		if(!empty($this->defs)) {
			foreach($this->defs as $namespace=>$values) {
				if(($namespace != 'twitter' or !empty($this->getParam('twitter'))) and is_array($values)) {
					foreach($values as $field=>$value) {
						if(!empty($value)) {
							if(!is_array($value)) {
								$content = htmlspecialchars($value, ENT_COMPAT, PLX_CHARSET);
?>
	<meta property="<?= $namespace ?>:<?= $field ?>" content="<?= $content ?>" />
<?php
							} else {
								foreach($value as $item) {
									$content = htmlspecialchars($item, ENT_COMPAT, PLX_CHARSET);
?>
	<meta property="<?= $namespace ?>:<?= $field ?>" content="<?= $content ?>" />
<?php
								}
							}
						}
					}
				}
			}
		}
?>
	<link rel="stylesheet" type="text/css" href="<?php echo self::COOKIE_CSS_URL; ?>" />
<?php
	}

	/**
	 * Add javascript at the bottom of the HTML page.
	 * Add Event listener at the toolbar of social buttons for getting the url of each social network.
	 * Open a popup window in response of the click on a social button.
	 *
	 * @return void
	 * */
	public function ThemeEndBody() {

		if($this->buttonsShow and !empty($this->defs)) {
			/** @ignore build javascript script.*/
			$accounts = array();
			foreach(self::ACCOUNT_NETWORKS as $network) {
				$value = trim($this->getParam('account_'.$network));
				if(!empty($value)) {
					$accounts[] = <<< EOT
	$network: '$value'
EOT;
				}
			}
			$networksList = array();
			foreach(explode('|', $this->getParam('networks')) as $network) {
				if(array_key_exists($network, $this->networks) and !in_array($network, array('mail', 'print'))) {
					list($url, $height, $width) = $this->networks[$network];
					$networksList[] = <<< EOT
	$network: {url: '$url', h: $height, w: $width}
EOT;
				}
			}

?>
	<script type="text/javascript"> // share_me plugin
	(function(theClassName) {

		'use strict';

		// building some useful constants
		const ACCOUNTS = {
<?php
	if(count($accounts) > 0) {
		echo "\t\t".implode(",\n\t\t", $accounts)."\n";
	}
?>
		};
		const NETWORKS = {
<?php
	echo "\t\t".implode(",\n\t\t", $networksList)."\n";
?>
		};

		// callback function in response on click event
		function popup(event) {
			if(event.target.tagName == 'IMG') {
				var network = event.target.getAttribute('data');
				if(network != null) {
					if(network in NETWORKS) {
						event.preventDefault();
						var datas = this.dataset;
						var nw = NETWORKS[network];
						var href = nw.url;
						var matches = href.match(/#\w+#/g);
						if(matches != null) {
							matches.forEach(function(tag) {
								if(tag == '#account#') {
									// for some network, it's better to give an account, e.g.: Twitter, Facebook
									if(network in ACCOUNTS) {
										href = href.replace(tag, ACCOUNTS[network], href);
									}
								} else {
									var key = tag.substring(1, tag.length-1);
									var newValue = (key in datas) ? datas[key] : '';
									href = href.replace(tag, newValue);
								}
							});
							href = encodeURI(href);
							var top = (screen.height - nw.h) / 2, left = (screen.width - nw.w) / 2;
							var options = 'menubar=no, toolbar=no, resizable=yes, scrollbars=no, width='+nw.w+', height='+nw.h+', top='+top+', left='+left;
							window.open(href, '', options);
						}
					} else {
						console.log('Unknown social network: ' + network);
					}
				}
			}
		};

		// Add eventListeners at every element with theClassName class
		const toolbars = document.getElementsByClassName(theClassName);
		if(toolbars.length > 0) {
			for(var i=0, iMax=toolbars.length; i<iMax; i++) {
				toolbars.item(i).addEventListener('click', popup);
			}
		}
	})('social-buttons');
	</script>
<?php
		}

	$this->_printCookieConsent();
	}

	/**
	 * It's a specific callback for this plugin.
	 * Add a toolbar of social buttons for each article or static page everywhere.
	 * Use it to customize the thema of your site.
	 *
	 * @return void
	 * */
	public function share_me($params=false) {

		global $plxMotor;

		if(empty($this->getParam('networks'))) {
			# any selected network by user
			echo "<!-- no network -->\n";
			return;
		}
		$this->buttonsShow = true;
		if (!in_array($plxMotor->mode, array('article', 'static'))) {
			$extras = explode('|', $this->getParam('extras'));
			if(!in_array('chapo', $extras) and in_array($plxMotor->mode, array('home', 'categorie', 'tags'))) {
				# Don't print the toolbar of networks is the article has a chapô anywhere
				$chapo = trim($plxMotor->plxRecord_arts->f('chapo'));
				if(!empty($chapo)) {
					return;
				}
			}
		}
		$datas = array(
			'lang' => $this->_getLocale($plxMotor->aConf['default_lang']),
			'site' => htmlspecialchars($plxMotor->aConf['title'], ENT_COMPAT, PLX_CHARSET)
		);
		switch($plxMotor->mode) {
			case 'article' :
			case 'static':
				foreach(array('url', 'title', 'description', 'image') as $field) {
					$datas[$field] = $this->defs['og'][$field];
				}
				$datas['tags'] = $this->defs['article']['tag'];
				break;
			case 'home' :
			case 'categorie' :
			case 'tags' :
				$article = $plxMotor->plxRecord_arts;
				$idArt = intval($article->f('numero'));
				$urlArt = $article->f('url');
				$tagsArt = explode(',', $article->f('tags'));
				$datas['url'] = $plxMotor->urlRewrite('?article'.$idArt.'/'.$urlArt);
				$datas['title'] = str_replace('"', '\"', $article->f('title'));
				$datas['tags'] = (!empty($tagsArt)) ? implode(',', array_map(function($tag) { return trim($tag); }, $tagsArt)) : '';
				foreach(array('chapo', 'meta_description') as $field) {
					if(!empty($article->f($field))) {
						$datas['description'] = $article->f($field);
						break;
					}
				}
				// Media
				if(is_string($params)) {
					$datas['image'] = $params;
				} elseif(!empty($article->f('thumbnail'))) {
					$datas['image'] = $plxMotor->aConf['racine'].str_replace('tb.', '', $article->f('thumbnail'));
				} else {
					$buf = $article->f('content');
					if(!empty($buf)) {
						$img = $this->_getImage($buf);
						if(!empty($img)) {
							$datas['image'] = $plxMotor->aConf['racine'].$img;
						}
					}
				}
				break;
			default:
		}

		$lines = array();
		foreach($datas as $key=>$value) {
			if(!empty($value)) {
				$lines[] = 'data-'.$key.'="'.htmlspecialchars($value, ENT_COMPAT, PLX_CHARSET).'"';
			}
		}
?>
		<ul class="social-buttons no-print"<?php if(!empty($lines)) { echo ' '.implode(' ', $lines); } ?>>
<?php
		foreach(explode('|', $this->getParam('networks')) as $network) {
			if(array_key_exists($network, $this->networks) and ($network != 'print') or in_array($this->mode, array('article', 'static'))) {
				$img = $this->iconNetwork($network);
				switch($network) {
					case 'mail':
						$innerHTML = '<a href="mailto:?subject='.$datas['title'].'&body='.$datas['url'].'">'.$img.'</a>';
						break;
					case 'print':
						$innerHTML = '<a href="#" onclick="window.print(); return false;">'.$img.'</a>';
						break;
					default:
						$innerHTML = $img;
				}
				$title = ucfirst($network);
				echo <<< BUTTON
				<li>$innerHTML</li>\n
BUTTON;
			}
		}
?>
		</ul>
<?php
	}

}
?>
