<?php
$LANG = array(
	'L_TAGS'		=> 'Partager les mot-clés',
	'L_SAVE'		=> 'Enregistrer',
	'L_MEDIA'		=> 'Image par défault',
	'L_MEDIA_TITLE'	=> 'parcourir le dossiers des médias',
	'L_OPENGRAPH'	=> 'En savoir plus sur le protocole Open Graph',
	'L_IMAGE_INFO'	=> 'A propos des dimensions des images',
	'L_OGP_DEBUGGER'=> 'Déboggueur Open Graph (par Facebook)',
	'L_CHAPO'		=> 'Filtrer les articles avec châpo',
	'L_DRAG_AND_DROP' => 'Modifier l\'ordre des réseaux en déplaçant les icônes ci-dessous avec la souris',
	'L_CHAPO_HINT'	=> 'Dans les pages d\'accueil, de catégories ou de mots-clés, ne pas afficher la barre des réseaux sociaux si l\'article a un chapô.',

	// Acceptation des cookies selon les lois européennes
	'L_COOKIE'		=> 'Demander pour les cookies',
	'L_COOKIE_POLICY'=> 'Page statique pour la politique des cookies',
	'L_COOKIE_POLICY_NO' => 'Aucune',
	'L_CC_MESSAGE'	=>  'Ce site utilise des cookies pour vous assurer une visite la plus enrichissante.',
	'L_CC_DISMISS'	=>  'Accepter !',
	'L_CC_ALLOW'	=>  'Autoriser les cookies',
	'L_CC_LINK'		=>  'En savoir plus'
);
?>
