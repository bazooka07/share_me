<?php
$LANG = array(
	'L_CC_ALLOW'					=> "Ativar os cookies",
	'L_CC_DISMISS'					=> "Aceitar",
	'L_CC_LINK'						=> "Saber mais",
	'L_CC_MESSAGE'					=> "Este site usa cookies para garantir que você obtenha a melhor experiência em nosso site",
	'L_CHAPO'						=> "Filtrar os artigos com cabeçalho",
	'L_CHAPO_HINT'					=> "Para páginas da página inicial, categorias e tags, não exiba botões de redes sociais se o artigo tiver um cabeçalho.",
	'L_COOKIE'						=> "Pergunte por cookies",
	'L_COOKIE_POLICY'				=> "Página estática para a política de cookies",
	'L_COOKIE_POLICY_NO'			=> "Não",
	'L_DRAG_AND_DROP'				=> "Classifique as redes movendo os ícones acima",
	'L_IMAGE_INFO'					=> "Sobre o tamanho das imagens",
	'L_MEDIA'						=> "Imagem padrão",
	'L_MEDIA_TITLE'					=> "Navegue pela pasta medias",
	'L_OGP_DEBUGGER'				=> "Depurador de gráfico aberto pelo Facebook",
	'L_OPENGRAPH'					=> "Saiba mais sobre o protocolo Open Graph",
	'L_SAVE'						=> "Salve ",
	'L_TAGS'						=> "Compartilhe as tags"
);
?>
